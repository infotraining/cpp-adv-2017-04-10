#pragma once
#include "bitmap.hpp"


void have_fun(const Bitmap & bmp)
{
	std::cout << "have_fun(const Bitmap&)" << std::endl;
	bmp.draw();
}

void have_fun(Bitmap & bmp)
{
	std::cout << "have_fun(Bitmap&)" << std::endl;
	bmp.draw();
}

void have_fun(Bitmap && bmp)
{
	std::cout << "have_fun(Bitmap&&)" << std::endl;
	bmp.draw();
}

template <typename B>
void use(B&& bmp)
{
	have_fun(std::forward<B>(bmp));
}
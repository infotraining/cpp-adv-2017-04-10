//#define D_SCL_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <vector>
#include <type_traits>

#include "prefect-forwarding.hpp"
#include "bitmap.hpp"

using namespace std;

struct Point
{
	int x, y;
};

class IShape
{
public:
	virtual void draw() const = 0;
	virtual void move(int dx, int dy) = 0;
	virtual ~IShape() = default;
};

class ShapeBase : public IShape
{
protected:
	Bitmap bmp_;
	Point pt_;
public:
	ShapeBase(const string& name, int x, int y) : bmp_{name, 25}, pt_{x, y}
	{}

	ShapeBase(ShapeBase&&) = default;
	ShapeBase& operator=(ShapeBase&&) = default;

	~ShapeBase()
	{
		std::cout << "~ShapeBase()" << std::endl;
	}

	void move(int dx, int dy) override
	{
		pt_.x += dx;
		pt_.y += dy;
	}
};

class Rectangle : public ShapeBase
{
private:
	int w_, h_;
public:
	Rectangle(int x, int y, int w, int h) : ShapeBase{"rect1", x, y}, w_{w}, h_{h}
	{}

	void draw() const override
	{
		bmp_.draw();
		cout << " at [" << pt_.x << ", " << pt_.y << "]" << endl;
	}
};


int main()
{
	{
		Bitmap bmp{ "drawing1", 20 };
		bmp.draw();

		Bitmap bmp_copy = bmp;

		cout << "bmp before move: " << bmp.name() << endl;
		cout << "bmp before move: " << bmp.mem_buffer() << endl;
		Bitmap bmp_moved = move(bmp);
		cout << "bmp after move: " << bmp.name() << endl;
		cout << "bmp after move: " << bmp.mem_buffer() << endl;

		cout << "\n----------------------\n";

		Sprite sprite("sprite1");

		Sprite sprite1_copy = sprite;
		Sprite sprite1_moved = move(sprite);

		cout << "\n----------------------\n";

		vector<Sprite> sprites;

		sprites.push_back(move(sprite1_moved));
		sprites.push_back(Sprite{ "sprite2" });
		sprites.push_back(Sprite{ "sprite3" });
		sprites.push_back(Sprite{ "sprite4" });
		sprites.push_back(create_sprite('$'));

		Sprite sprite2 = create_sprite('@');

		//string name = "sp1";

		cout << "\n--------\n";

		Rectangle rect{ 10, 20, 100, 200 };
		rect.draw();

		cout << "\n--------\n";

		Rectangle rect_moved = move(rect);
		rect_moved.draw();

		cout << "\n----------------------\n";

		auto str1 = string("text");
		auto str2 = str1;

		const vector<string> words = { "one", "two", "three" };

		auto _1 = words[0];
		auto& _2 = words[0];
		auto&& _3 = words[0];

		vector<bool> vec_bool = { 0, 1, 1, 1, 0 };			

		for (auto&& bit : vec_bool)
			bit.flip();

		for (const auto& item : vec_bool)
			cout << item << " ";
		cout << endl;	

		cout << "\n----------------------\n";

		const Bitmap bmpA{ "const bmpA", 20 };
		Bitmap bmpB{ "bmpB",20 };
		
		cout << "\n";

		use(bmpA);
		use(bmpB);
		use(Bitmap{ "temp bmpC", 20 });

		system("PAUSE");
	}
}

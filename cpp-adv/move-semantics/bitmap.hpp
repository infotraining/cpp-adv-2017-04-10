#pragma once
#include <string>
#include <iostream>

class Bitmap
{
	std::string name_ = "unknown";
	char* image_ = nullptr;
	size_t size_;
public:
	Bitmap(const std::string& name, size_t size, char fill_char = '+')
		: name_{ name }, image_{ new char[size + 1] }, size_{ size }
	{
		std::cout << "Ctor Bitmap(" << name_ << ")" << std::endl;
		std::fill(image_, image_ + size, fill_char);
		image_[size] = '\0';
	}

	Bitmap(const Bitmap& src)
		: name_{ src.name_ }, image_{ new char[src.size_ + 1] }, size_{ src.size_ }
	{
		std::cout << "Ctor Bitmap(const Bitmap& " << name_ << ")" << std::endl;

		std::copy(src.image_, src.image_ + size_ + 1, image_);
	}

	Bitmap& operator=(const Bitmap& src)
	{
		Bitmap temp(src);
		swap(temp);

		std::cout << "op=(const Bitmap& " << name_ << ")" << std::endl;

		return *this;
	}

	Bitmap(Bitmap&& src) noexcept
		: name_{ move(src.name_) }, image_{std::move(src.image_) }, size_{std::move(src.size_) }
	{
		src.image_ = nullptr;
		src.size_ = 0;

		std::cout << "Ctor Bitmap(Bitmap&& " << name_ << ")" << std::endl;
	}

	Bitmap& operator=(Bitmap&& src) noexcept(std::is_nothrow_move_assignable<std::string>::value)
	{
		if (this != &src)
		{
			delete[] image_;

			name_ = move(src.name_);
			image_ = std::move(src.image_);
			size_ = std::move(src.size_);

			// set to resourceless state			
			src.image_ = nullptr; // ***
			src.size_ = 0;
		}

		std::cout << "op=(Bitmap&& " << name_ << ")" << std::endl;

		return *this;
	}

	void swap(Bitmap& other)
	{
		name_.swap(other.name_);
		std::swap(image_, other.image_);
		std::swap(size_, other.size_);
	}

	~Bitmap()
	{
		delete[] image_;
	}

	void draw() const
	{
		std::cout << "Drawing " << name_ << " - [" << image_ << "]" << std::endl;
	}

	void* mem_buffer() const
	{
		return image_;
	}

	std::string name() const
	{
		return name_;
	}
};

class Sprite
{
	Bitmap bmp_;

public:
	explicit Sprite(const std::string& name, char fill_char = '*') : bmp_{ name, 20, fill_char }
	{}

	void draw() const
	{
		bmp_.draw();
	}
};

Sprite create_sprite(char fill_char)
{
	static int id = 0;

	Sprite temp = Sprite{ "sprite" + std::to_string(++id), fill_char };
	return temp;
}

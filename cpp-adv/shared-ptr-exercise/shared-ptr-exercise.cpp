#include <iostream>
#include <set>
#include <cassert>
#include <cstdlib>
#include <stdexcept>
#include <string>
#include <memory>

class Observer
{
public:
	virtual void update(const std::string& event_args) = 0;
	virtual ~Observer() {}
};

class Subject
{
	int state_;
	std::set<std::weak_ptr<Observer>, std::owner_less<std::weak_ptr<Observer>>> observers_;

public:
	Subject() : state_(0)
	{}

	void register_observer(std::weak_ptr<Observer> observer)
	{
		observers_.insert(observer);
	}

	void unregister_observer(std::weak_ptr<Observer> observer)
	{
		observers_.erase(observer);
	}

	void set_state(int new_state)
	{
		if (state_ != new_state)
		{
			state_ = new_state;
			notify("Changed state on: " + std::to_string(state_));
		}
	}

protected:
	void notify(const std::string& event_args)
	{
		auto it = observers_.begin();

		while(it != observers_.end())
		{
			auto sp = it->lock();

			if (sp)
			{
				sp->update(event_args);
				++it;
			}
			else
				it = observers_.erase(it);
		}
	}
};

class ConcreteObserver1 : public Observer, public std::enable_shared_from_this<ConcreteObserver1>
{
public:
	virtual void update(const std::string& event)
	{
		std::cout << "ConcreteObserver1: " << event << std::endl;
	}

	void register_at_subject(Subject& s)
	{
		s.register_observer(shared_from_this());
	}
};

class ConcreteObserver2 : public Observer
{
public:
	virtual void update(const std::string& event)
	{
		std::cout << "ConcreteObserver2: " << event << std::endl;
	}
};

int main(int argc, char const *argv[])
{
	using namespace std;

	Subject s;

	auto o1 = make_shared<ConcreteObserver1>();
	o1->register_at_subject(s);

	{
		auto o2 = make_shared<ConcreteObserver2>();
		s.register_observer(o2);

		s.set_state(1);		

		cout << "End of scope." << endl;
	}

	s.set_state(2);

	system("PAUSE");
}

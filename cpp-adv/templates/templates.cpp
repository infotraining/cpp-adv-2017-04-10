#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T>
const T& maximum(const T& a , const T& b)
{
	return a > b ? a : b;
}

const char* maximum(const char* a, const char* b)
{
	return strcmp(a, b) > 1 ? a : b;
}

template <typename T, size_t N>
T* Begin(T (&arr)[N])
{
	return &(arr[0]);
}

template <typename T, size_t N>
T* End(T(&arr)[N])
{
	return &(arr[0]) + N;
}

void foo(int* tab)
{
	
}

void foo(int (&tab)[10])
{

}




class X
{
	vector<int> data_;
public:
	X(int a, string b)
	{
		cout << "X(int, string)" << endl;
	}

	X(initializer_list<int> items) : data_{items}
	{
		//for(const auto& item : items)
		//{
		//	data_.push_back(item);
		//}

		cout << "X(il<int>)" << endl;
	}
};

void auto_type_deduction()
{
	int tab[10];

	auto guess1 = tab; // int*
	auto& guess2 = tab; // int(&)[10]

	const int a = 10;
	const int& ref_a = 10;
	
	auto guess3 = a; // int
	auto guess4 = ref_a;

	auto& guess5 = a;  // const int&
	auto& guess6 = ref_a; // const int&	
}

void initializer_lists()
{
	vector<int> vec = { 1, 2, 3, 4, 5 }; // vector<int>(initializer_list<int>)

	auto guess7 = { 1 }; // initializer_list<int>	

	X x{ 1, "text"s };

	for(const auto& i : { 1, 2, 3 })
	{
		cout << i << " ";
	}
	cout << endl;
}

int main()
{
	auto r1 = maximum(1, 9);

	cout << r1 << endl;

	cout << maximum(6.3, 66.545) << endl;

	cout << maximum<double>(5, 4.8) << endl;

	cout << maximum("ala", "ola") << endl;

	int tab[5] = { 1, 2, 3, 4, 5 };
	int tab2[5];

	copy(Begin(tab), End(tab), Begin(tab2));

	auto_type_deduction();

	system("PAUSE");
}
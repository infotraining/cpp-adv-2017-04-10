#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <memory>
#include <functional>
#include <deque>
#include <array> 

using namespace std;

class Gadget
{
	string name_ = "unknown gadget";
	int sn_ = 0;
protected:
	auto tied() const
	{
		return tie(name_, sn_);
	}
public:
	Gadget() = default;

	explicit Gadget(const string& name) : name_{name}
	{}

	Gadget(Gadget&&) = default;
	Gadget& operator=(Gadget&&) = default;

	virtual ~Gadget() = default;

	string name() const
	{
		return name_;
	}

	void close()
	{
		std::cout << name_ << " disposed...\n";
	}

	bool operator==(const Gadget& g) const
	{
		return this->tied() == g.tied();
	}
};

class MockGadget : public Gadget
{
	function<void()> callable_;
public:
	explicit MockGadget(std::function<void()> callable) : callable_{callable}
	{}

	~MockGadget()
	{
		callable_();
	}
};

std::unique_ptr<Gadget> create_gadget(const string& name)
{
	auto ptr = make_unique<Gadget>(name);
	return ptr;
}

TEST_CASE("unique_ptr")
{
	SECTION("default constructor creates an empty pointer")
	{
		std::unique_ptr<Gadget> ptr;

		REQUIRE(ptr.get() == nullptr);
		REQUIRE(ptr == nullptr);
	}

	SECTION("can be initialized with pointer to dynamic object")
	{
		unique_ptr<Gadget> ptr{ new Gadget() };

		REQUIRE(ptr.get() != nullptr);
	}

	SECTION("works like pointer")
	{
		unique_ptr<Gadget> ptr{ new Gadget{"ipad"} };

		REQUIRE(ptr->name() == "ipad");
		REQUIRE((*ptr).name() == "ipad");
	}

	SECTION("has factory make_unique() since C++14")
	{
		unique_ptr<Gadget> ptr1 = make_unique<Gadget>("ipad");
		auto ptr2 = make_unique<Gadget>("ipad");

		REQUIRE(ptr1->name() == "ipad");
		REQUIRE(ptr2->name() == "ipad");
	}

	SECTION("it destroys previous object")
	{
		bool is_destroyed = false;

		unique_ptr<Gadget> ptr = make_unique<MockGadget>([&is_destroyed] { is_destroyed = true; });

		SECTION("when assigned")
		{
			ptr = make_unique<Gadget>();

			REQUIRE(is_destroyed);
		}

		SECTION("when reset() called")
		{
			ptr.reset();

			REQUIRE(is_destroyed);
		}
	}

	SECTION("is nocopyable")
	{
		unique_ptr<Gadget> ptr = make_unique<Gadget>();
		//auto ptr2 = ptr;  // error
	}

	SECTION("is moveable")
	{
		unique_ptr<Gadget> ptr1 = make_unique<Gadget>();
		
		auto ptr2 = move(ptr1); 

		REQUIRE(ptr1 == nullptr);
		REQUIRE(ptr2 != nullptr);
	}

	SECTION("can be returned from function")
	{
		auto ptr = create_gadget("mp3 player");

		REQUIRE(ptr->name() == "mp3 player");
	}

	SECTION("can be stored in STL containers")
	{
		vector<unique_ptr<Gadget>> gadgets;

		gadgets.push_back(create_gadget("1"));
		gadgets.push_back(create_gadget("1"));

		auto ptr = create_gadget("3");

		gadgets.push_back(move(ptr));

		for(const auto& g : gadgets)
		{
			cout << g->name() << endl;
		}		

		ptr = make_unique<Gadget>("4");
	}

	SECTION("supports custom deallocators")
	{
		SECTION("unsafe code")
		{
			//FILE* file = fopen("text.tmp", "w");

			// may_throw();

			//fclose(file);
		}

		SECTION("safe with custom dealloc")
		{
			//unique_ptr<FILE, int(*)(FILE*)> file{ fopen("text.tmp", "w"), &fclose };

			// may_throw();
		}

		SECTION("custom deallocator defined with a lambda")
		{
			auto gadget_closer = [](Gadget* g) {
				g->close();
				delete g;
			};

			decltype(gadget_closer) b = gadget_closer;

			unique_ptr<Gadget, decltype(gadget_closer)> g{ new Gadget("7"), gadget_closer };
		}
	}
}


void print(const string& text, int& count)
{
	std::cout << text << std::endl;
	++count;
}

class Printer
{
public:
	void operator()(const string& text, int& count)
	{
		std::cout << text << std::endl;
		++count;
	}
};

class Lambda_r23742
{
public:
	template <typename T1, typename T2>
	void operator()(const T1& text, T2& counter) const
	{
		cout << text << endl;
		++counter;
	}
};

void foo(int) {}

void legacy_code(int* arr, size_t size)
{
	
}

Gadget* allocate_large_table()
{
	Gadget* tab = new Gadget[10];

	return tab;
}

TEST_CASE("C-style arrays are deprecated")
{
	SECTION("stack allocated array - use std::array instead")
	{
		int tab[100] = {};

		array<int, 100> arr = {1, 2, 3, 4};

		REQUIRE(arr.size() == 100);

		legacy_code(arr.data(), arr.size());
	}

	SECTION("dynamic array - use unique_ptr<T[]>")
	{
		Gadget* tab = new Gadget[10];

		delete[] tab;

		unique_ptr<Gadget[]> arr{allocate_large_table()};

		arr[0].name();
	}
}

TEST_CASE("std::function")
{
	int counter = 0;

	function<void(const string&, int&)> f;

	SECTION("can store and call function ptr")
	{
		f = &print;

		f("text", counter);

		REQUIRE(counter == 1);
	}

	SECTION("can store and call functor")
	{
		Printer prn;
		f = prn;

		f("text", counter);

		REQUIRE(counter == 1);
	}

	SECTION("can store and call lambda")
	{
		int& c = counter;
		
		f = [](const auto& text, auto& counter) {
			cout << text << endl;
			++counter;
		};

		f("text", c);

		REQUIRE(counter == 1);
	}

	SECTION("can be used to work queue")
	{
		deque<function<void()>> q;

		q.push_back([&counter] { ++counter; });
		q.push_back([&counter] { counter++; });
		q.push_back([&counter] { ++counter; });
	}

	SECTION("prefer auto than std::function")
	{
		vector<int> vec1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		vector<int> vec2 = { 1, 2, 3, 4, 5, 6, 7, 8 };

		int threshold = 5;
		// function<bool(int)> auto gt_threshold = [threshold](int x) { return x > threshold; };  // avoid if you can
		
		auto gt_threshold = [threshold](int x) { return x > threshold; };
		
		REQUIRE(count_if(vec1.begin(), vec1.end(), gt_threshold) == 5);
		REQUIRE(count_if(vec2.begin(), vec2.end(), gt_threshold) == 3);
	}
}

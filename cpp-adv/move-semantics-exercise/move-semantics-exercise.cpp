#include <thread>
#include <string>
#include <iostream>

using namespace std;

void background_work(int id, const std::string& text, std::chrono::milliseconds interval)
{
	std::cout << "Start of THD#1" << std::endl;

	for (const auto& c : text)
	{
		std::cout << c;
		std::this_thread::sleep_for(interval);
		std::cout << " ";
	}
}

int main()
{
	std::thread thd1{ &background_work, 1, "move", 200ms };
	std::thread thd2{ [] { background_work(2, "semantics", 100ms); } };

	thd1.join();
	thd2.join();

	system("PAUSE");
}
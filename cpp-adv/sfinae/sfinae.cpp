#include <type_traits>
#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;

template <typename T>
typename enable_if<sizeof(T) < 8>::type do_stuff(T obj)
{
	cout << "do_stuff(small obj)" << endl;
}


// 1st version
template <typename T>
enable_if_t<(sizeof(T) >= 8)> do_stuff(const T& obj)
{
	cout << "do_stuff(large obj)" << endl;
}

// alternative take
//template <typename T>
//void do_stuff(const T& obj, typename enable_if<(sizeof(T) >= 8)>::type* ptr = 0)
//{
//	cout << "do_stuff(large obj)" << endl;
//}

struct Shape
{};

struct Rect : Shape
{};

template <typename T>
using IsShape = std::enable_if_t<std::is_base_of<Shape, T>::value>;

template <
	typename T,
	typename = IsShape<T>
>
void do_other_stuff_with_shape(T const& shape)
{
	cout << "using shape" << endl;
}

// sfinae

template <typename T, typename Enabled = void>
struct Data
{
public:
	Data()
	{
		cout << "Data(generic)" << endl;
	}
};

template <typename T>
using FloatingPoint = std::enable_if_t<std::is_floating_point<T>::value>;

template <
	typename T
>
class Data<T, FloatingPoint<T>>
{
public:
	Data()
	{
		cout << "Data(floating point)" << endl;
	}
};

template <typename InIter, typename OutIter>
void mcopy(InIter start, InIter end, OutIter dest)
{
	for(auto it = start; it != end; ++it, ++dest)
	{
		*dest = *it;
	}

	cout << "Generic mcopy" << endl;
}

template <typename T>
constexpr bool IsMemCopyable_v = is_trivially_copyable<T>::value;

template <typename T1, typename T2>
constexpr bool AreTheSame_v = is_same<remove_const_t<T1>, remove_const_t<T2>>::value;

template <typename Src, typename Dest,
	typename = enable_if_t<IsMemCopyable_v<Src> && AreTheSame_v<Src, Dest>>>
void mcopy(Src* start, Src* end, Dest* dest)
{
	std::cout << "mcopy - optimized version with memcpy" << std::endl;

	auto length = end - start;
	memcpy(dest, start, length * sizeof(Src));
}

int main()
{
	do_stuff(8);

	do_stuff("text"s);

	do_stuff(3.14);

	Rect rect;
	do_other_stuff_with_shape(rect);
	do_other_stuff_with_shape<string, int>("text"s);

	Data<string> data_str;
	Data<double> data_dbl;

	vector<int> vec = { 1, 2, 3, 4, 5 };
	list<int> lst(5);

	mcopy(vec.begin(), vec.end(), lst.begin());

	const int tab1[5] = { 1, 2, 3, 4, 5 };
	int tab2[5];

	mcopy(begin(tab1), end(tab1), begin(tab2));

	system("PAUSE");
}
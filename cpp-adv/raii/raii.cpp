#include <mutex>
#include <thread>
#include <iostream>

using namespace std;

int counter = 0;
std::mutex mtx_counter;

class RaiiThread
{
	std::thread thd_;
public:
	//template <typename Arg>
	//explicit RaiiThread(Arg&& arg) : thd_{std::forward<Arg>(arg)}
	//{		
	//}

	template <typename Arg1, typename Arg2>
	RaiiThread(Arg1&& arg1, Arg2&& arg2) : thd_{ std::forward<Arg1>(arg1), std::forward<Arg2>(arg2) }
	{
		std::cout << "A" << std::endl;
	}

	template <typename... Args>
	RaiiThread(Args&&... args) : thd_{ std::forward<Args>(args)... }
	{		
		std::cout << "B" << std::endl;
	}

	RaiiThread(const RaiiThread&) = delete;
	RaiiThread& operator=(const RaiiThread&) = delete;

	RaiiThread(RaiiThread&&) = delete;
	RaiiThread& operator=(RaiiThread&&) = delete;

	~RaiiThread()
	{
		if (thd_.joinable())
			thd_.join();
	}
		
};

void may_throw()
{
	throw std::runtime_error("E#13");
}

void background_work(int id)
{
	std::lock_guard<std::mutex> lk{ mtx_counter };

	//may_throw();
	counter++;	
}


int main()
{
	{
		thread thd1{ &background_work, 1 };
		RaiiThread thd1_guard{ std::move(thd1) };


		RaiiThread thd2{ [] { background_work(2); } };

		RaiiThread thd3{ &background_work, 3 };

		//may_throw();	
	}

	system("PAUSE");
}
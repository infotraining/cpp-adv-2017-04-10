#include <type_traits>
#include <memory>
#include <string>

static_assert(sizeof(int) == 4, "Bad platform");

template <typename T, T v>
struct IntegralConstant
{
	static constexpr T value = v;
};

template <bool v>
using BoolConstant = IntegralConstant<bool, v>;

using TrueType = BoolConstant<true>;
using FalseType = BoolConstant<false>;

//------------------------------------------------------
template <typename T>
struct IsVoid : FalseType
{	
};

template <>
struct IsVoid<void> : TrueType
{	
};

template <typename T>
constexpr bool IsVoid_v = IsVoid<T>::value;

//template <typename T>
//constexpr bool Is_Void()
//{	
//	return false;
//}
//
//template <>
//constexpr bool Is_Void<void>()
//{
//	return true;
//}

template <typename T>
void foo(T* t)
{
	static_assert(!IsVoid_v<T>, "Error: T* can't be void*");
	//static_assert(!Is_Void<T>(), "Error");

	//static_assert(!std::is_void_v<T>, "Error: T* can't be void*");
}

//-----------------------------------------------------

template <typename T>
struct is_auto_ptr : std::false_type
{};

template <typename T>
struct is_auto_ptr<std::auto_ptr<T>> : std::true_type
{};

template <typename Pointer>
void use(Pointer ptr)
{
	static_assert(!is_auto_ptr<Pointer>::value, "auto_ptr is not compatibile");
}

//------------------------------------------------------
//-----------------------------------------------------
struct SubstitutionFailure // tag
{};

template <typename T>
struct SubstitutionSucceeded : std::true_type
{};

template <>
struct SubstitutionSucceeded<SubstitutionFailure> : std::false_type
{};

template <typename T>
struct Plus_Operator_Oracle
{
private:
	template <typename X>
	static auto check(const X& x) -> decltype(x + x);

	static SubstitutionFailure check(...);
public:
	using type = decltype(check(std::declval<T>()));
};

template <typename T>
struct HasPlusOperator : SubstitutionSucceeded<typename Plus_Operator_Oracle<T>::type>
{};

struct Number
{
	int value;
};

Number operator+(const Number& a, const Number& b)
{
	return{ a.value + b.value };
}

int main()
{
	static_assert(IntegralConstant<int, 5>::value == 5, "Error");
	static_assert(BoolConstant<true>::value == true, "Error");

	void* raw_mem = nullptr;
	//foo(raw_mem); // static_assert failed

	int* int_ptr{};
	foo(int_ptr);

	static_assert(HasPlusOperator<std::string>::value, "No +");
	static_assert(HasPlusOperator<Number>::value, "No +");
}
#include <iostream>

using namespace std;

enum class Bitmask { b0 = 0x1, b1 = 0x2, b2 = 0x4 };

constexpr Bitmask operator|(Bitmask left, Bitmask right)
{
    return Bitmask(static_cast<int>(left) | static_cast<int>(right) ); 
}

int main()
{
    Bitmask b = Bitmask::b1 | Bitmask::b2;

    switch (b)
    {
        case Bitmask::b1 | Bitmask::b2:
            cout << "Bitmask::b1 | Bitmask::b2 - " << static_cast<int>(b) << endl;
        default:
            cout << "Other case...";
    }
}
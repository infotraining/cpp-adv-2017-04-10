#include <iostream>
#include <utility>
#include <string_view>
#include <algorithm>

using namespace std;

namespace Constexpr
{
    template<typename I, typename E, typename P>
    constexpr I find_if(I it, E end, P pred)
    {
        while (it != end && !pred(*it)) 
            ++it;

        return it;
    }

    template<typename In1, typename In2>
    constexpr bool equal(In1 it1, In1 end1, In2 it2, In2 end2)
    {
        while (it1 != end1)
        {
            if (*it1 != *it2)
                return false;

            ++it1;
            ++it2;
        }    

        return it2 == end2;
    }

    template <typename T>
    struct Matcher1st
    {
        T value;

        template <typename P>
        constexpr bool operator()(P const& p) const
        {
            return value == p.first;
        }
    };

    template <>
    struct Matcher1st<std::string_view>
    {
        std::string_view value;

        template <typename P>
        constexpr bool operator()(P const& p)
        {
            return equal(std::begin(value), std::end(value), std::begin(p.first), std::end(p.first));
        }
    };

    template <typename T>
    constexpr Matcher1st<T> match1st(T value)
    {
        return Matcher1st<T>{ value };
    }

    template <typename T, size_t N, typename Key>
    auto constexpr access(T const (&a)[N], Key key)
    {
        T const* it = Constexpr::find_if(std::begin(a), std::end(a), match1st(key));
        return it != end(a) ? it->second : throw std::out_of_range("key not found");
    }
}

int main()
{
    constexpr pair<string_view, int> map[] = {
        pair<string_view, int>{"one"sv, 1},
        make_pair("two"sv, 2),
        {"three"sv, 3}
    };


    constexpr int three = Constexpr::access(map, "three"sv);

    int value = 2;

    switch (value)
    {
        case Constexpr::access(map, "one"sv):
            cout << "one" << endl;
            break;
        case map[1].second:
            cout << "two" << endl;
            break;
        case three:
            cout << "three" << endl;
    }

    int four1 = Constexpr::access(map, "four"sv); // compiles and throws an exception
    //constexpr int four2 = Constexpr::access(map, "four"sv); // doesn't compile
}

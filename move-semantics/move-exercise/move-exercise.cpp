#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cassert>


using namespace std;

class Bitmap
{
	string name_ = "unknown";
	char* image_ = nullptr;

public:
	Bitmap(const string& name, size_t size, char fill_character = '+') : name_(name)
	{
		assert(size >= 1);

		cout << "Ctor Bitmap(" << name_ << ")" << endl;
		image_ = new char[size];
		fill(image_, image_ + size - 1, fill_character);
		image_[size - 1] = 0;
	}

	~Bitmap()
	{
		cout << "Dtor Bitmap(" << name_ << ")" << endl;
		delete[] image_;
	}

	void draw() const	
	{
		cout << "Drawing " << name_ << ": ";

		if (image_)
			cout << image_ << endl;
	}
};

Bitmap* create_bitmap()
{
	static int gen_id;

	Bitmap* bmp = new Bitmap{ "bmp_from_factory" + to_string(++gen_id), 20, '*' };

	return bmp;
}

int main()
{
	Bitmap* ptr_bmp1 = create_bitmap();

	ptr_bmp1->draw();

	delete ptr_bmp1;
}
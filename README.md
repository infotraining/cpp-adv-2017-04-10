# Ankieta #
* https://docs.google.com/a/infotraining.pl/forms/d/e/1FAIpQLScuhZnbP0avBkcrEK0GXObeZYxf3CVBHo58znqOMpr9n3MOqA/viewform?hl=pl

# Links #

* https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#S-resource
* https://github.com/Microsoft/GSL
* [MinGW gcc 6.3](https://nuwen.net/mingw.html)
* [Catch - Testing framework](https://github.com/philsquared/Catch/blob/master/docs/tutorial.md)
* [Mocks](https://github.com/rollbear/trompeloeil/blob/master/docs/CookBook.md)
* [Leak Freedom by Default](https://www.youtube.com/watch?v=JfmTagWcqoE)
* [Writing C++14 by default](https://www.youtube.com/watch?v=hEx5DNLWGgA&t=1506s)

# Better task system #
* https://www.youtube.com/watch?v=QIHy8pXbneI